const app = require('express')(),
    bodyParser = require('body-parser'),
    routes = require('./routes/')


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use((err, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method')
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETED,DELETE,OPTIONS')
    res.header('Allow', 'GET,POST,PUT,DELETED,DELETE,OPTIONS')

    next()
})

// Van las rutas de la aplicación
app.use(routes)

module.exports = app
