function modelAsset(callback) {
    let asset = {
        idGovernment:'',
        cct:'',
        typeAsset:'',
        description:'',
        dateUpdate: new Date(),
        dateAssign: new Date(),
        price: 0
    }

    callback(asset)
}

module.exports = {
    modelAsset
}
