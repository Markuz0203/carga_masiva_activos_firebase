'user strict'
const api = require('express').Router(),
      constrollers = require('../controllers/'),
      multer = require('multer'),
      path = require('path'),
      upload = multer({ dest: path.join(__dirname, '/../uploads/') })

api.post('/uploadFile', upload.single('file'), constrollers.excelToJsonStates)
api.get('/getAssets', constrollers.getAssets)
//api.post('/newUploadFile',upload.single('file'), constrollers.newExcel)

module.exports = api


