'use strict'
const converterExcel = require('excel-as-json').processFile,
    fs = require('fs'),
    async = require('async'),
    path = require('path'),
    firebase = require('firebase'),
    { modelAsset } = require('../models/index'),
    uid = require('uid'),
    chunk = require('lodash.chunk')
require('firebase/firestore')

let options = {
    sheet: '1',
    isColOriented: false,
    omitEmtpyFields: false
}

function excelToJsonStates(req, res) {
    if (req.file) {
        converterExcel(path.join(__dirname, `/../uploads/${req.file.filename}`), undefined, options, (err, data) => {
            let assets = []
            if (!err) {
                modelAsset((asset) => {
                    let documents = (chunk(data, 500))
                    let index = 0
                    async.eachOfLimit(documents, 10, (documentt, key, callback) => {
                        const batch = firebase.firestore().batch()
                        async.forEachOf(documentt, (value, key, callbackk) => {
                            const id = uid(10)
                            const activesRef = firebase.firestore().collection("assetsFurniture").doc(id)
                            handleAssingData(asset, value, (asset) => {
                                index++
                                console.log('saved: ', index)
                                batch.set(activesRef, asset);
                                callbackk()
                            })
                        }, err => {
                            if (!err) {
                                batch.commit().then(() => {
                                    callback()
                                })
                            }
                        })
                    }, error => {
                        console.log("Termine de guardar en la BD");
                        // res.status(200).send({ message: 'Termine de guardar en la BD' })
                    })
                })
            } else {
                console.log('Error al guardar..')
                // res.status(500).send({ message: err })
            }
        })
    } else {
        // res.status(404).send({ message: 'No se encotro archivo' })
    }

    // if (req.file) {

    //     converterExcel(path.join(__dirname, `/../uploads/${req.file.filename}`), undefined, options, (err, data) => {
    //         let activos = []
    //         let i = 1
    //         let j = 1

    //         modelAsset(asset => {

    //             data.forEach(value => {
    //                 handleAssingData(asset, value, (activo) => {
    //                     activos = activos.concat(activo)
    //                 })
    //             });

    //         })

    //         activos.forEach(activo => {
    //             firebase.firestore().collection('assetsFurniture').add(activo).then(() => {
    //                 console.log('activo guardado', i)

    //                 i = i + 1;
    //             }).catch(error => console.log(error))
    //         });

    //         res.status(200).send({
    //             message: 'Termine de guardar en la BD',
    //             activosSaved: activos.length
    //         })
    //     })

    // } else {
    //     res.status(404).send({ message: 'No se encotro archivo' })
    // }
}

function getAssets(req, res) {
    // firebase.firestore().collection('assetsFurniture').get().then(data => {
    //     console.log(data.docs.length)
    // })

    firebase.firestore().collection('assetsFurniture').orderBy("idGovernment", "desc").limit(100).onSnapshot(data =>{
        let activos = []
        data.forEach(active =>{
            activos = activos.concat(active.data())
        })

        res.status(200).send({ 
            message: 'Se limpia el cache con esta petición',
            activos: activos 
        })
    })
}


function inserthree(activo) {
    firebase.firestore().collection('assetsFurniture').add(activo).then(() => {
        callback(true)
    }).catch(error => console.log(error))
}


function handleAssingData(asset, value, callback) {
    asset.idGovernment = value.NO_SEIEM
    asset.cct = value.CCT
    asset.typeAsset = value.MUEBLE
    asset.description = value.CARACTERISTICAS
    asset.dateUpdate = new Date(value.FECHA_ALTA)
    asset.dateAssign = new Date(value.FECHA_ASIGNACION)
    asset.price = value.PRECIO
    callback(asset)
}

module.exports = {
    excelToJsonStates,
    getAssets
}